//alert("Hello, Batch249");
/*
let students = ["John", "Joe", "Jane", "Jessie"];

console.log(students);

//What array method can we use to add an item at the end of the array?

students.push("Raf");

console.log(students);

students.push("Pinky");

console.log(students);

students.pop();
console.log(students);

students.shift();
console.log(students);

*/
//What is the difference between splice() and slice method()?

/*
	splice - changes the original array
		   - mutator methods
		   - remove and adding items from a starting index

	slice - Non-Mutator methods
		  - copies a portion from starting index and returns a new array from it

	
	Iterator Methods
		- loop over the items of an array

	forEach()
	map()
	every()
*/

let arrNum = [15, 20, 25, 30, 11, 7];

/*
	Using forEach, check if every item in the array is divisible by 5. If they are, log in the console "<num> is divisble by 5" if not, log false.
	
*/

arrNum.forEach(function(number) {
	if(number % 5){
		console.log("False");
	}
	else
		console.log(number + " is divisible by 5")
	
});


//Math Object - allows you to perform mathematical tasks on numbers
//Mathematical constants
console.log(Math);
console.log(Math.E); // Eular's number
console.log(Math.PI); // PI
console.log(Math.SQRT2) // square root of 2
console.log(Math.SQRT1_2); // square root of 1/2
console.log(Math.LN2);// natural logarithm of 2
console.log(Math.LN10); //natural logarith of 10
console.log(Math.LOG2E); //base 2 of logarith of E 
console.log(Math.LOG10E); // base 10 of logarithm of E

//Methods for rouding a number to an interger
console.log(Math.round(Math.PI)); //3
console.log(Math.ceil(Math.PI)); //4
console.log(Math.floor(Math.PI));
console.log(Math.trunc(Math.PI));

//returns a square root of a number
console.log(Math.sqrt(3.14)); 

console.log(Math.min(-1, -2, -4, -100, 0, 1, 3, 5, 6));

console.log(Math.max(-1, -2, -4, -100, 0, 1, 3, 5, 6));


const number = 5 ** 2;

console.log(number);

console.log(Math.pow(5, 2));



//primative
// string, number, null, boolean, undefined

//.length - property
//.toUpperCase() - method



//Activity;

/*
1. const array_name = [item1, item2]
2. array_name[0]
3. array_name[array_name.length - 1]
4. indexOf()
5. forEach()
6. map()
7. every()
8. some()
9. true
10. true

*/

let students = ["John", "Joe", "Jane", "Jessie"];

let addToEnd = (students_arr, student) => {
	if(typeof student === 'string'){
		students_arr.push(student);
		return console.log(students);
	}
	else
		return "error - can only add strings to an array"
}


let addToStart = (students_arr, student) => {
	if(typeof student === 'string'){
		students_arr.unshift(student);
		return console.log(students);
	}
	else
		return "error - can only add strings to an array";
}

let elementChecker = (students_arr, student) => {
	if(students_arr.includes(student)){
		return true;
	}
	else if (students_arr.length === 0){
		return console.log("error - passed in array is empty");
	}
	else 
		return console.log("No data");
}


let checkAllStringsEnding = (students_arr, student) => {
	for(let i = 0; i < students_arr.length; i++){
		if(typeof students_arr[i] != 'string') {
			return console.log("error - all array elements must be strings");
		}
	}


	if(students_arr.length === 0){
		return console.log("error - array must NOT be empty");
	}
	else if (typeof student != 'string'){
		return console.log("error - 2nd argument must be of data type string")
	}
	else if(students.length !== 1) {
		return console.log("error - 2nd argument must be a single character");
	}
}

let stringLengthSorter = (students_arr, student) => {
	for(let i = 0; i < students_arr.length; i++){
		if(typeof students_arr[i] != 'string') {
			return console.log("error - all array elements must be strings");
		}
	}

	return console.log(students_arr.sort());
}